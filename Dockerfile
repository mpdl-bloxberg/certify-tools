FROM docker.io/tiangolo/uvicorn-gunicorn-fastapi:python3.8
USER 0

ADD . /app/cert_tools
ADD ./certify-api/app/controller /app/controller
ADD ./certify-api/app/controller/tools_application.py /app/main.py

EXPOSE 80
WORKDIR /app/cert_tools

ADD ./run.sh /run.sh

RUN pip install -r requirements.txt

CMD /run.sh
